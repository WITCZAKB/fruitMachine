$(document).ready(function(){

/*Common Functions*/

    function updateGame(){
        $("#userBalance").html(userBalance);
        $("#roundBalance").html(roundBalance);
        $("#jackpot").html(jackpot + " &euro;");
        $.cookie('userBalance', userBalance, { expires: 7 });
        $.cookie('roundBalance', roundBalance, { expires: 7 });
        $.cookie('jackpot', jackpot, { expires: 7 });

        //Update Prize board
        if(roundBalance != 0){

            if(roundBalance>= betMax){

                $("li").find("#cashPrize").each(function(index){
                    var grade = parseInt($(this).parent().find("#grade").text());
                    $(this).html(((roundBalance* (roundBalance/2) * grade)+40) + " &euro;");
                });

                $(".jackpot").html(((roundBalance* (roundBalance/2) * 8)+jackpot)+ " &euro;");
                $(".rank1").html(((roundBalance * (roundBalance/2))+10)+" &euro;");
            }else{
                $("li").find("#cashPrize").each(function(index){
                    var grade = parseInt($(this).parent().find("#grade").text());
                    $(this).html(((roundBalance * grade)+40) + " &euro;");
                });

                $(".jackpot").html(((roundBalance * 8)+jackpot)+ " &euro;");
                $(".rank1").html(((roundBalance)+10)+" &euro;");
            }



        }else{
            $("li").find("#cashPrize").each(function(index){
                $(this).text(0);
            });
        }

    }


    //Init
    function init(){
        controlsDisable(false);
        betMax = 10;
        defaultBalance = 100;
        defaultRoundBalance = 0;
        defaultJackpot = function(){return Math.floor((Math.random()*5000)+5000);}

        //The balance of the user
        userBalance = $.cookie('userBalance') ? parseInt($.cookie('userBalance')) : defaultBalance;

        //The balance during a round
        roundBalance = $.cookie('roundBalance') ? parseInt($.cookie("roundBalance")) : defaultRoundBalance;

        //The balance Jackpot
        jackpot = $.cookie('jackpot') ? parseInt($.cookie("jackpot")) : defaultJackpot();


        //Sound Configuration
        bigWinSound = document.createElement('audio');
        //bigWinSound.setAttribute('src', 'sound/bigWin.mp3');

        winSound = document.createElement('audio');
        //winSound.setAttribute('src', 'sound/win.mp3');

        betSound = document.createElement('audio');
        //betSound.setAttribute('src', 'sound/bet.mp3');

        spinSound = document.createElement('audio');
        //spinSound.setAttribute('src', 'sound/spin.mp3');


        jackpotSound = document.createElement('audio');
        //jackpotSound.setAttribute('src', 'sound/jackpot.mp3');

        updateGame();

    }

    //Calculate the bet
    function bet(amount){

         if(userBalance-amount >= 0 ){
            $("#message").html("");
            betSound.play();
            userBalance -= amount;
            roundBalance += amount;
            jackpot += amount;

            updateGame();
        }
    }

    //Reset the game, delete all save
    function resetGame(){
        userBalance = 100;
        roundBalance = 0;
        jackpot = defaultJackpot();

        $.removeCookie("userBalance");
        $.removeCookie("roundBalance");
        $.removeCookie("jackpot");


        updateGame();
    }

    //Do the animation
    //Return the amount if the player win
    function spin(results){

        var spinData = [];

        if(results[0]==0){
            spinData = calculatePrice(1);
        }else{
            if(results[2] == results[1] && results[1] == results[0]){
                spinData = calculatePrice(results[1]+2);
            }else{
                spinData = calculatePrice(-1);
            }
        }

        if(spinData[1] != 0){
            //Switch on the grade

            switch (spinData[0]){
                case 8:
                    //Jackpot !!
                    jackpotSound.play();
                    defaultJackpot();
                    setTimeout(function() {
                        controlsDisable(false);
                    }, 34410);
                    $("#message").html("Jackpot !!!");
                    break;
                case 1:
                    winSound.play();
                    setTimeout(function() {
                        controlsDisable(false);
                    }, 3000);
                    $("#message").html("Gains : "+spinData[1]+"&euro;");
                    break;
                default:
                    bigWinSound.play();
                    setTimeout(function() {
                        controlsDisable(false);
                    }, 10500);

                    $("#message").html("Gains : "+spinData[1]+"&euro;");
                    break;
            }

            roundBalance = 0;
            updateGame();

        }else{
            //Lose !!
            $("#message").html("Perdu !!");
            roundBalance =0;
            setTimeout(function() {
                controlsDisable(false);
            }, 400);
            updateGame();
        }

    }


    //Calculate the amount of the prize
    function calculatePrice(grade){

        var prize = 0;
        switch(grade){
            case 1:
                if(roundBalance >=betMax){
                    prize = (roundBalance* (roundBalance/2)) + 10;
                }else{
                    prize = (roundBalance * grade) + 10;
                }
                userBalance += prize;
            break;

            case -1:

            break;

            case 8:
                prize = jackpot;
                if(roundBalance >= betMax){
                    userBalance = (userBalance + roundBalance* (roundBalance/2)*7) + jackpot;

                }else{
                    userBalance = (userBalance + roundBalance*7) + jackpot;
                }
                jackpot = defaultJackpot();
            break;

            default:
                if(roundBalance >= betMax) {
                    prize = (roundBalance*roundBalance* (roundBalance/2) * grade) + 40;
                }else{
                    prize = (roundBalance * grade) + 40;

                }
                userBalance = userBalance + prize;
            break;
        }

        roundBalance = 0;
        updateGame();

        return [grade, prize];
    }

    //Return a number which represent the three slot
    function getResults(){
        return [Math.floor(Math.random() * 7), Math.floor(Math.random() * 7), Math.floor(Math.random() * 7)];
    }

    //Cancel all bet for this round
    function cancel(){
        userBalance += roundBalance;
        jackpot -= roundBalance;
        roundBalance = 0;
        updateGame();
    }

    //Enable or disable controls
    function controlsDisable(disable){
        $("#spin").prop("disabled", disable);

        $("#betMax").prop("disabled", disable);
        $("#bet1").prop("disabled", disable);
        $("#cancel").prop("disabled", disable);
    }


/*& Common Functions*/

/*Event Listener*/
    //Bet 1
    $("#bet1").click(function(){
        bet(1);
    });

    $("#betMax").click(function(){
        $("#cancel").trigger("click");
        bet(betMax);
        $("#spin").trigger("click");
    });



    //Spin
    $("#spin").click(function(){

        controlsDisable(true);

        if(roundBalance == 0 && userBalance-1 >= 0){
            bet(1);
        }

        if(roundBalance != 0){

            spinSound.play();
            $("#message").html("");
            var results = getResults();

            $("#panelSpin").find("img").each(function(index){


                $("#panelSpin").find("#spin"+index).attr("src", "images/spin"+index+".gif");
                var soundDeclencher = [2742, 3074, 3503];

             setTimeout(function() {
                    $("#spin"+index).attr("src", "images/res"+results[index]+".jpg");
                }, soundDeclencher[index]);

            });

            //Launch the price calcul after a certain time with animation;
            setTimeout(function() {
                spin(results);

            }, 3900);
        }


    });

    $("#resetGame").click(function(){
        resetGame();
    });

    $("#cancel").click(function(){
        cancel();
    });
/*& Event Listener*/


    init();


});